#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <sys/time.h>  
#include <curl/curl.h> 
#include "sqlite3.h" 
#include "mysession.h"

char* getcgidata(FILE* fp, char* requestmethod);    //获得cgi数据

int main()
{
	char *input; 
	char *req_method; 
	char fname[128];        //名
	char fvalue[128];       //值
	int i = 0; 
	int j = 0; 
	
	FILE *fp_html;
	char buf[512]="";
	FILE *fp_text;
	
	char session_id[17];	
	
	//
	char ways1[32];
	char ways2[32];
	char session_id_t[32];
	char page[32];
	char request[32];
	
	char part1[48];
	char part2[48];
	char part3[48];
	char part4[48];
	
	char temp[32];
	
	int i_section;
	
	int i_page = 1;
	
	req_method = getenv("REQUEST_METHOD");          //方法
	input = getcgidata(stdin, req_method);          //输入
	
	//-----------------------------------------------------------------------
	for ( i = 0; i < (int)strlen(input); i++ ) 
	{ 
		if ( input[i] == '=' ) 
		{ 
					 fname[j] = '\0'; 
					 break; //
		}                                    
		fname[j++] = input[i]; 
	} 
	// + "="
	for ( i = 1 + strlen(fname), j = 0; i < (int)strlen(input); i++ ) 
	{ 
		fvalue[j++] = input[i]; 
	} 
	fvalue[j] = '\0';
	j = 0;
	
	if(strcmp(fname,"session_id")==0)
	{
		stpcpy(session_id,fvalue);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作

		if( (fp_html=fopen("../tp/tp_data_manage_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");				
			}
			else
				printf("%s",buf);
			
		}	
	}
	else if(strcmp(fname,"ways1")==0)
	{
		for ( i = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part1[j] = '\0'; 
						 break; //
			}                                    
			part1[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 1 + strlen(part1), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part2[j] = '\0'; 
						 break; //
			} 
			part2[j++] = fvalue[i]; 
		} 
		////------------------------
		for ( i = 0; i < (int)strlen(part1); i++ ) 
		{ 
			if ( part1[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part1[i]; 
		} 
		// + "="
		for ( i = 0 + strlen(temp), j = 0; i < (int)strlen(part1); i++ ) //特别
		{ 
			ways1[j++] = part1[i]; 
		} 
		ways1[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part2); i++ ) 
		{ 
			if ( part2[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part2[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part2); i++ ) 
		{ 
			session_id_t[j++] = part2[i]; 
		} 
		session_id_t[j] = '\0';
		j = 0;
		memset(temp,0,32);
		
		stpcpy(session_id,session_id_t);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作

		if( (fp_html=fopen("../tp/tp_data_manage_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");
				
				//数据库查询
				
				//------------------------------------------------------------------------------------
		
				sqlite3 *db = NULL; 
				char *zErrMsg = 0; 
				int rc; 
				char sql[128];
				
				sqlite3_stmt *stmt;
				int ret=0;
				
				int cn_current_msg;
				int cn_xg_record;
				int cn_device_requested;
				int cn_device_unrequested;
				int cn_note_registered;
				int cn_card_registered;
				
				rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开数据库
				
				//选择sql
				if(strcmp(ways1,"s0")==0)	
				{			
					i_section = 0;
					
					strcpy(sql,"select count(*) from current_msg;");
					ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
					if (ret !=SQLITE_OK)
					{
						return -1;
					}
					ret = sqlite3_step(stmt);
					cn_current_msg = sqlite3_column_int(stmt, 0);
				//	sqlite3_finalize(stmt);
					memset(sql,0,128);
					
					strcpy(sql,"select count(*) from xg_record;");
					ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
					if (ret !=SQLITE_OK)
					{
						return -1;
					}
					ret = sqlite3_step(stmt);
					cn_xg_record = sqlite3_column_int(stmt, 0);
				//	sqlite3_finalize(stmt);
					memset(sql,0,128);
					
					strcpy(sql,"select count(*) from device_requested;");
					ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
					if (ret !=SQLITE_OK)
					{
						return -1;
					}
					ret = sqlite3_step(stmt);
					cn_device_requested = sqlite3_column_int(stmt, 0);
				//	sqlite3_finalize(stmt);
					memset(sql,0,128);
					
					strcpy(sql,"select count(*) from device_unrequested;");
					ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
					if (ret !=SQLITE_OK)
					{
						return -1;
					}
					ret = sqlite3_step(stmt);
					cn_device_unrequested = sqlite3_column_int(stmt, 0);
				//	sqlite3_finalize(stmt);
					memset(sql,0,128);
					
					strcpy(sql,"select count(*) from note_registered;");
					ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
					if (ret !=SQLITE_OK)
					{
						return -1;
					}
					ret = sqlite3_step(stmt);
					cn_note_registered = sqlite3_column_int(stmt, 0);
				//	sqlite3_finalize(stmt);
					memset(sql,0,128);
					
					strcpy(sql,"select count(*) from card_registered;");
					ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
					if (ret !=SQLITE_OK)
					{
						return -1;
					}
					ret = sqlite3_step(stmt);
					cn_card_registered = sqlite3_column_int(stmt, 0);
					sqlite3_finalize(stmt);
					memset(sql,0,128);
				}
				else if(strcmp(ways1,"s1")==0)    //导出
				{
					i_section = 1;
					
					system("/bin/sh /myapps/dbs/sql_backup.sh");
				}
				else if(strcmp(ways1,"s2")==0)    //上传
				{
					i_section = 2;
					
					//----------------------------------------------------------------
					CURL *curl;  
			  
					CURLM *multi_handle;  
					int still_running;  

					struct curl_httppost *formpost=NULL;  
					struct curl_httppost *lastptr=NULL;  
					struct curl_slist *headerlist=NULL;  
					static const char buf[] = "Expect:";  

					/* Fill in the file upload field. This makes libcurl load data from 
					 the given file name when curl_easy_perform() is called. */  
					curl_formadd(&formpost,  
							   &lastptr,  
							   CURLFORM_COPYNAME, "sendfile",  
							   CURLFORM_FILE, "sql_backup.sql",  
							   CURLFORM_END);  

					/* Fill in the filename field */  
					curl_formadd(&formpost,  
							   &lastptr,  
							   CURLFORM_COPYNAME, "filename",  
							   CURLFORM_COPYCONTENTS, "sql_backup.sql",  
							   CURLFORM_END);  

					/* Fill in the submit field too, even if this is rarely needed */  
					curl_formadd(&formpost,  
							   &lastptr,  
							   CURLFORM_COPYNAME, "submit",  
							   CURLFORM_COPYCONTENTS, "send",  
							   CURLFORM_END);  

					curl = curl_easy_init();  
					multi_handle = curl_multi_init();  

					/* initalize custom header list (stating that Expect: 100-continue is not 
					 wanted */  
					headerlist = curl_slist_append(headerlist, buf);  
					if(curl && multi_handle) {  

					/* what URL that receives this POST */  
					curl_easy_setopt(curl, CURLOPT_URL, "http://weixlink.applinzi.com/data_upload.php");  
					curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);  

					curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);  
					curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);  

					curl_multi_add_handle(multi_handle, curl);  

					curl_multi_perform(multi_handle, &still_running);  

					do {  
					  struct timeval timeout;  
					  int rc; /* select() return code */  

					  fd_set fdread;  
					  fd_set fdwrite;  
					  fd_set fdexcep;  
					  int maxfd = -1;  

					  long curl_timeo = -1;  

					  FD_ZERO(&fdread);  
					  FD_ZERO(&fdwrite);  
					  FD_ZERO(&fdexcep);  

					  /* set a suitable timeout to play around with */  
					  timeout.tv_sec = 1;  
					  timeout.tv_usec = 0;  

					  curl_multi_timeout(multi_handle, &curl_timeo);  
					  if(curl_timeo >= 0) {  
						timeout.tv_sec = curl_timeo / 1000;  
						if(timeout.tv_sec > 1)  
						  timeout.tv_sec = 1;  
						else  
						  timeout.tv_usec = (curl_timeo % 1000) * 1000;  
					  }  

					  /* get file descriptors from the transfers */  
					  curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);  

					  /* In a real-world program you OF COURSE check the return code of the 
						 function calls.  On success, the value of maxfd is guaranteed to be 
						 greater or equal than -1.  We call select(maxfd + 1, ...), specially in 
						 case of (maxfd == -1), we call select(0, ...), which is basically equal 
						 to sleep. */  

					  rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);  

					  switch(rc) {  
					  case -1:  
						/* select error */  
						break;  
					  case 0:  
					  default:  
						/* timeout or readable/writable sockets */  
						printf("perform!\n");  
						curl_multi_perform(multi_handle, &still_running);  
						printf("running: %d!\n", still_running);  
						break;  
					  }  
					} while(still_running);  

					curl_multi_cleanup(multi_handle);  

					/* always cleanup */  
					curl_easy_cleanup(curl);  

					/* then cleanup the formpost chain */  
					curl_formfree(formpost);  

					/* free slist */  
					curl_slist_free_all (headerlist);  
					}
					
					//----------------------------------------------------------------
				}
				else if(strcmp(ways1,"s3")==0)    //删除
				{
					i_section = 3;
					
					system("/bin/sh /myapps/dbs/sql_delete.sh");
				}
				else if(strcmp(ways1,"s4")==0)    //清空
				{
					i_section = 4;
					
					strcpy(sql,"delete from current_msg;");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,128);
					
					strcpy(sql,"delete from xg_record;");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,128);
					
					strcpy(sql,"delete from device_requested;");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,128);
					
					strcpy(sql,"delete from device_unrequested;");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,128);
					
					strcpy(sql,"delete from note_registered;");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,128);
					
					strcpy(sql,"delete from card_registered;");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,128);
				}
				else 
					;
				
				//分类
				switch(i_section)
				{
					case 0:  //统计、导出、上传、删除、清空
						printf("<p>1、统计信息</p>\n");
						printf("<p>当前数据库：zigbee.db</p>\n");
						printf("<table id='Table1' border='1' width='1000'>\n<tr>\n<td>表格</td>\n<td>current_msg</td>\n<td>xg_record</td>\n<td>device_requested</td>\n<td>device_unrequested</td>\n<td>note_registered</td>\n<td>card_registered</td>\n</tr>\n");					
						printf("<tr>\n<td>记录</td>\n<td>%d</td>\n<td>%d</td>\n<td>%d</td>\n<td>%d</td>\n<td>%d</td>\n<td>%d</td>\n<tr>\n",cn_current_msg,cn_xg_record,cn_device_requested,cn_device_unrequested,cn_note_registered,cn_card_registered);					
						printf("</table>\n");
						
						//--
						printf("<p>2、导出整个数据库为sql文件：</p>\n");
						printf("<form method='post'>\n");					
						printf("<input type='hidden' name='ways1' value='s1'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						//--
						printf("<p>3、将导出的数据库sql文件上传至服务器保存：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s2'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						//--
						printf("<p>4、删除导出的数据库sql文件：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s3'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						//--
						printf("<p>5、清空当前数据库中的所有表格：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s4'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						
						break;

					case 1:  //导出、上传、删除、清空
						printf("<p>1、返回信息：</p>\n");
						printf("已导出整个数据库为sql文件！");
						
						//--
						printf("<p>2、将导出的数据库sql文件上传至服务器保存：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s2'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						//--
						printf("<p>3、删除导出的数据库sql文件：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s3'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						//--
						printf("<p>4、清空当前数据库中的所有表格：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s4'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						break;
						
					case 2:  //上传、删除、清空
						printf("<p>1、返回信息：</p>\n");
						printf("已将导出的数据库sql文件上传至服务器保存！");
						
						//--
						printf("<p>2、删除导出的数据库sql文件：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s3'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						//--
						printf("<p>3、清空当前数据库中的所有表格：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s4'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						break;
						
					case 3:  //删除、清空
						printf("<p>1、返回信息：</p>\n");
						printf("已删除导出的数据库sql文件！");
						
						//--
						printf("<p>2、清空当前数据库中的所有表格：</p>\n");
						printf("<form method='post'>\n");
						printf("<input type='hidden' name='ways1' value='s4'/>\n");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='submit' name='request' value='确定' />\n");
						printf("</form>");
						
						break;
						
					case 4:  //清空
						printf("<p>1、返回信息：</p>\n");
						printf("已清空当前数据库中的所有表格！");
						
						break;
				}
				
				
				//------------------------------------------------------------------------------------
				
				
			}
			else
				printf("%s",buf);
			
		}	
	}
	else
		;
 
	return 0; 
         
} 




char* getcgidata(FILE* fp, char* requestmethod) 
{ 
	char* input; 
	int len; 
	int size = 1024; 
	int i = 0; 
	
	if (!strcmp(requestmethod, "GET")) 
	{ 
		input = getenv("QUERY_STRING"); 
		return input; 
	} 
	else if (!strcmp(requestmethod, "POST")) 
	{ 
		len = atoi(getenv("CONTENT_LENGTH")); 
		input = (char*)malloc(sizeof(char)*(size + 1)); 
	 
		if (len == 0) 
		{ 
			input[0] = '\0'; 
			return input; 
		} 
	 
		while(1) 
		{ 
			input[i] = (char)fgetc(fp); 
			if (i == size) 
			{ 
				input[i+1] = '\0'; 
				return input; 
			} 
			
			--len; 
			if (feof(fp) || (!(len))) 
			{ 
				i++; 
				input[i] = '\0'; 
				return input; 
			} 
			i++; 
			
		} 
	} 
	return NULL; 
}
