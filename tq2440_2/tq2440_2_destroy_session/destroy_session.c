#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "sqlite3.h" 
#include "mysession.h"

char* getcgidata(FILE* fp, char* requestmethod);    //获得cgi数据

int main()
{
	char *input; 
	char *req_method; 
	char fname[40];        //名
	char fvalue[40];       //值
	int i = 0; 
	int j = 0; 
	
	FILE *fp_html;
	char buf[512]="";
	
	char session_id[17];	
	
	req_method = getenv("REQUEST_METHOD");          //方法
	input = getcgidata(stdin, req_method);          //输入
	
	//-----------------------------------------------------------------------
	for ( i = 0; i < (int)strlen(input); i++ ) 
	{ 
		if ( input[i] == '=' ) 
		{ 
					 fname[j] = '\0'; 
					 break; //
		}                                    
		fname[j++] = input[i]; 
	} 
	// + "="
	for ( i = 1 + strlen(fname), j = 0; i < (int)strlen(input); i++ ) 
	{ 
		fvalue[j++] = input[i]; 
	} 
	fvalue[j] = '\0';
	j = 0;
	
	if(strcmp(fname,"session_id")==0)
	{
		stpcpy(session_id,fvalue);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		
		if( (fp_html=fopen("../tp/tp_index_html2","r")) == NULL)
		{
			exit(1);
		}
		
		while(fgets(buf,512,fp_html))
		{
			printf("%s",buf);
		}
		
		kill_session(session_id);
	}
	 
	return 0;   //返回
}


char* getcgidata(FILE* fp, char* requestmethod) 
{ 
	char* input; 
	int len; 
	int size = 1024; 
	int i = 0; 
	
	if (!strcmp(requestmethod, "GET")) 
	{ 
		input = getenv("QUERY_STRING"); 
		return input; 
	} 
	else if (!strcmp(requestmethod, "POST")) 
	{ 
		len = atoi(getenv("CONTENT_LENGTH")); 
		input = (char*)malloc(sizeof(char)*(size + 1)); 
	 
		if (len == 0) 
		{ 
			input[0] = '\0'; 
			return input; 
		} 
	 
		while(1) 
		{ 
			input[i] = (char)fgetc(fp); 
			if (i == size) 
			{ 
				input[i+1] = '\0'; 
				return input; 
			} 
			
			--len; 
			if (feof(fp) || (!(len))) 
			{ 
				i++; 
				input[i] = '\0'; 
				return input; 
			} 
			i++; 
			
		} 
	} 
	return NULL; 
}